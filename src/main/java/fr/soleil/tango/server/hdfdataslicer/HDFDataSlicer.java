package fr.soleil.tango.server.hdfdataslicer;

import java.io.File;
import java.lang.reflect.Array;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.cdma.Factory;
import org.cdma.dictionary.Key;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IGroup;
import org.cdma.plugin.soleil.nexus.NxsFactory;
import org.cdma.utils.IArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.AttributeProperties;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.Status;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.lib.project.math.ArrayUtils;

@Device
public class HDFDataSlicer {

    private final Logger logger = LoggerFactory.getLogger(HDFDataSlicer.class);
    private static final String IMAGES = "images";
    private static final String DATA = "data";
    private static final String SCAN = "scan";
    private static final String ESCALE = "escale";
    private static final String XSCALE = "xscale";
    private static final String TRAJECTORY = "x_position";

    private final Key imagesKey = new Key(NxsFactory.getInstance(), IMAGES);
    private final Key scanKey = new Key(NxsFactory.getInstance(), SCAN);
    private final Key dataKey = new Key(NxsFactory.getInstance(), DATA);
    private final Key eScaleKey = new Key(NxsFactory.getInstance(), ESCALE);
    private final Key xScaleKey = new Key(NxsFactory.getInstance(), XSCALE);
    private final Key trajectoryKey = new Key(NxsFactory.getInstance(), TRAJECTORY);

    private Thread currentFileScanner = createCurrentFileScannerThread();
    private IDataset dataSet = null;

    @DeviceProperty(defaultValue = "storage/recorder/datarecorder.1")
    private String dataRecorderDeviceName;

    @DeviceProperty(defaultValue = "/usr/Local/CDMADictionaryRoot/")
    private String cdmaDictionaryFolder;

    @Attribute(name = "File")
    private String fileName = "";

    @Attribute(name = "Folder")
    private String folderName = "";

    @Attribute(displayLevel = DispLevel._EXPERT)
    @AttributeProperties(unit = " ")
    private int xIndex;
    @Attribute(displayLevel = DispLevel._EXPERT)
    @AttributeProperties(unit = " ")
    private int yIndex;
    @Attribute(displayLevel = DispLevel._EXPERT)
    @AttributeProperties(unit = " ")
    private int zIndex;

    @Attribute(name = "TrajAngle")
    private int[][] sliceX;
    @Attribute(name = "EnergyTraj")
    private int[][] sliceY;
    @Attribute(name = "EnergyAngle")
    private int[][] sliceZ;

    @Attribute
    private double[] angles;
    @Attribute
    private double angle_min;
    @Attribute
    private double angle_max;
    @Attribute
    private double[] energies;
    @Attribute
    private double energy_min;
    @Attribute
    private double energy_max;
    @Attribute
    private double[] trajectory;
    @Attribute
    private double trajectory_min;
    @Attribute
    private double trajectory_max;

    @Attribute(name = "mbsAngle")
    @AttributeProperties(format = "%8.4f", unit = " ")
    private double angle;

    @Attribute(name = "kE")
    @AttributeProperties(format = "%8.4f", unit = " ")
    private double ke;

    @Attribute(name = "trajectoryValue")
    @AttributeProperties(format = "%8.4f", unit = " ")
    private double trajectoryValue;

    @Attribute(name = "Auto Update File Name")
    private boolean autoUpdateFileName = true;

    @State
    private DeviceState state;
    @Status
    private String status;
    private boolean initialized = false;
    private IArray imagesArray;
    private String[] messages = new String[3];
    private boolean[] statuses = new boolean[3];

    public int getXIndex() {
        return xIndex;
    }

    public int getYIndex() {
        return yIndex;
    }

    public int getZIndex() {
        return zIndex;
    }

    public void setXIndex(int xIndex) {
        this.xIndex = xIndex;
        this.sliceX = null;
        this.sliceX = computeSlice(xIndex, 2, "Slice X");
    }

    public void setYIndex(int yIndex) {
        this.yIndex = yIndex;
        this.sliceY = null;
        this.sliceY = computeSlice(yIndex, 1, "Slice Y");
    }

    public void setZIndex(int zIndex) {
        this.zIndex = zIndex;
        this.sliceZ = null;
        this.sliceZ = computeSlice(zIndex, 0, "Slice Z");
    }

    private void computeAll() {
        this.sliceX = computeSlice(xIndex, 2, "Slice X");
        this.sliceY = computeSlice(yIndex, 1, "Slice Y");
        this.sliceZ = computeSlice(zIndex, 0, "Slice Z");
    }

    private int[][] computeSlice(int index, int rank, String sliceName) {
        int[][] result = null;
        try {
            setState(DeviceState.RUNNING);
            IArrayUtils arrayUtils = imagesArray.getArrayUtils().slice(rank, index);
            int[] shape = arrayUtils.getArray().getShape();
            int[] inlineArray = (int[]) arrayUtils.copyTo1DJavaArray();
            result = (int[][]) ArrayUtils.convertArrayDimensionFrom1ToN(inlineArray, shape);

        } catch (Exception e) {
            logger.error("Impossible to compute Slice " + sliceName, e);
            result = null;
        }
        if (result == null) {
            statuses[rank] = false;
            messages[rank] = (sliceName + " calculation @ index = " + index + " failed.");
        } else {
            statuses[rank] = true;
            messages[rank] = (sliceName + " calculation @ index = " + index + " done.");
        }

        updateStatusAndState();
        return result;
    }

    private void updateStatusAndState() {
        StringBuilder builder = new StringBuilder();
        builder.append((getFileName() != "") ? "File = " + getFileName() + "\n\n" : "");
        for (int i = 0; i < messages.length; i++) {
            builder.append(messages[i] + "\n");
        }
        setStatus(builder.toString());
        if (org.apache.commons.lang3.ArrayUtils.contains(statuses, false)) {
            setState(DeviceState.FAULT);
        } else {
            setState(DeviceState.ON);
        }

    }

    public boolean isAutoUpdateFileName() {
        return autoUpdateFileName;
    }

    public void setAutoUpdateFileName(boolean autoUpdateFileName) {
        this.autoUpdateFileName = autoUpdateFileName;
    }

    // ***** SLICEs ******//
    public int[][] getSliceX() {
        return sliceX;
    }

    public int[][] getSliceY() {
        return sliceY;
    }

    public int[][] getSliceZ() {
        return sliceZ;
    }

    // *********** Scales (ANGLES, ENERGIES, TRAJECTORY) ********//

    public double[] getAngles() {
        return angles;
    }

    public double[] getEnergies() {
        return energies;
    }

    public double[] getTrajectory() {
        return trajectory;
    }

    // *************ke *******//

    public double getAngle_min() {
        return angle_min;
    }

    public double getAngle_max() {
        return angle_max;
    }

    public double getEnergy_min() {
        return energy_min;
    }

    public double getEnergy_max() {
        return energy_max;
    }

    public double getTrajectory_min() {
        return trajectory_min;
    }

    public double getTrajectory_max() {
        return trajectory_max;
    }

    public double getKe() {
        return ke;
    }

    // Energy is related image array dimension 0
    public void setKe(double ke) {
        this.ke = ke;
        int index = getClosestIndexToValue(energies, ke);
        setXIndex(index);
    }

    // ************* MbsAngle *******//
    public double getAngle() {
        return angle;
    }

    // Energy is related image array dimension 1
    public void setAngle(double mbsAngle) {
        this.angle = mbsAngle;
        int index = getClosestIndexToValue(angles, mbsAngle);
        setYIndex(index);
    }

    // ************* TrajectoryValue *******//
    public double getTrajectoryValue() {
        return trajectoryValue;
    }

    // Trajectory Value is related image array dimension 2
    public void setTrajectoryValue(double trajectoryValue) {
        this.trajectoryValue = trajectoryValue;
        int index = getClosestIndexToValue(trajectory, trajectoryValue);
        setZIndex(index);
    }

    // closestIndex n'est jamais Ã  -1 s'il y'a un fichier!
    private int getClosestIndexToValue(double[] source, double goalValue) {
        int closestIndex = -1;
        if (source != null) {
            double closestValue = Double.NaN;
            int index = 0;
            for (double value : source) {
                if (Double.isNaN(closestValue)
                        || (Math.abs(closestValue - goalValue) > (Math.abs(value - goalValue)))) {
                    closestValue = value;
                    closestIndex = index;
                }
                index++;
            }
        }
        return closestIndex;
    }

    // ***********filename *****//
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileNameToSet) {
        this.fileName = fileNameToSet;// "/nfs/ruche-antares/antares-soleil/com-antares/2017/Run3/Sophie/T411/T411_In_2017-06-28_14-58-06.nxs";
        if (loadFile()) {
            computeAll();
        }
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
        if (loadFile()) {
            computeAll();
        }
    }

    public DeviceState getState() throws DevFailed {
        return state;
    }

    public void setState(DeviceState state) {
        this.state = state;
    }

    public void setDataRecorderDeviceName(String dataRecorderDeviceName) {
        this.dataRecorderDeviceName = dataRecorderDeviceName;
    }

    public String getDataRecorderDeviceName() {
        return this.dataRecorderDeviceName;
    }

    public void setCdmaDictionaryFolder(String folder) {
        this.cdmaDictionaryFolder = folder;
    }

    public boolean loadFile() {
        boolean result = false;
        Factory.setDictionariesFolder(cdmaDictionaryFolder);

        IDataset physicalDataset = null;

        // ---------------------------------------------
        // Close existing dataset(s) if any before opening again the file for
        // updated data
        if (dataSet != null) {
            try {
                dataSet.close();
            } catch (Exception e) {
                logger.error("System Error: Cannot close logical file dataset");
            }
        }
        // ---------------------------------------------
        String completeFileName = folderName + File.separator + fileName;
        // Open file is physical mode in order to read the nxentry
        try {

            physicalDataset = Factory.openDataset(new URI("file://" + completeFileName));

        } catch (Exception e) {
            setState(DeviceState.FAULT);
            setStatus("=> File " + new File(fileName).getName() + " is not readeable.");
            return result;
        }
        try {
            IGroup root = physicalDataset.getRootGroup();
            Collection<IGroup> groupList = root.getGroupList();
            ArrayList<IGroup> groupArrayList = new ArrayList<>(groupList);
            IGroup nxEntry = groupArrayList.get(root.getGroupList().size() - 1);
            physicalDataset.close();

            String uriValue = "file://" + completeFileName + "#" + nxEntry.getShortName();
            URI uri = new URI(uriValue);

            dataSet = Factory.openDataset(uri);
            LogicalGroup lroot = dataSet.getLogicalRoot("data_reduction");
            LogicalGroup scanGroup = lroot.getGroup(scanKey);
            LogicalGroup dataGroup = scanGroup.getGroup(dataKey);
            IDataItem imagesDataItem = dataGroup.getDataItem(imagesKey);

            if (imagesDataItem == null) {
                setState(DeviceState.FAULT);
                setStatus("No images found in file");
                return result;
            }
            IDataItem escaleDataItem = dataGroup.getDataItem(eScaleKey);
            if (escaleDataItem == null) {
                setState(DeviceState.FAULT);
                setStatus("EScale value is missing in file");
                return result;
            }
            IDataItem xscaleDataItem = dataGroup.getDataItem(xScaleKey);
            if (xscaleDataItem == null) {
                setState(DeviceState.FAULT);
                setStatus("XScale value is missing in file");
                return result;
            }
            IDataItem trajectoryDataItem = dataGroup.getDataItem(trajectoryKey);
            if (trajectoryDataItem == null) {
                setState(DeviceState.FAULT);
                setStatus("Trajectory value is missing in file");
                return result;
            }

            Object xScale = xscaleDataItem.getData().getArrayUtils().copyToNDJavaArray();
            if (xScale instanceof double[][]) {
                double[][] xScale2d = (double[][]) xScale;
                angles = xScale2d[0];
            } else {
                angles = (double[]) xScale;
            }
            angle_min = angles[0];
            angle_max = angles[angles.length - 1];

            Object eScale = escaleDataItem.getData().getArrayUtils().copyToNDJavaArray();
            if (eScale instanceof double[][]) {
                double[][] eScale2d = (double[][]) eScale;
                energies = eScale2d[0];

            } else {
                energies = (double[]) eScale;
            }
            energy_min = energies[0];
            energy_max = energies[energies.length - 1];

            Object trajectoryArray = trajectoryDataItem.getData().getArrayUtils().copyToNDJavaArray();
            if (trajectoryArray instanceof double[][]) {
                List<Double> trajectoryList = flatten(trajectoryArray);
                trajectoryArray = trajectoryList.toArray(new Double[trajectoryList.size()]);
                trajectory = (double[]) org.apache.commons.lang3.ArrayUtils.toPrimitive(trajectoryArray);

            } else {
                trajectory = (double[]) trajectoryArray;
            }
            trajectory_min = trajectory[0];
            trajectory_max = trajectory[trajectory.length - 1];

            this.imagesArray = imagesDataItem.getData();

            if (!initialized) {
                setAngle(0);
                setKe(0);
                setTrajectoryValue(0);
                initialized = true;
            }

            result = true;
        } catch (Exception e) {
            setState(DeviceState.FAULT);
            setStatus("Error while extracting data from file :" + e.getMessage());
        } finally {
            try {
                if (physicalDataset != null) {
                    physicalDataset.close();
                }
            } catch (Exception ioe) {
                logger.error("System Error: Cannot close physical file dataset");
            }
        }

        return result;
    }

    private static List<Double> flatten(Object object) {
        List<Double> l = new ArrayList<>();
        if (object.getClass().isArray()) {
            for (int i = 0; i < Array.getLength(object); i++) {
                l.addAll(flatten(Array.get(object, i)));
            }
        } else if (object instanceof List) {
            for (Object element : (List<?>) object) {
                l.addAll(flatten(element));
            }
        } else {
            l.add((double) object);
        }
        return l;
    }

    /**
     * init device
     * 
     */
    @Init
    public void init() {
        initialized = false;
        setState(DeviceState.INIT);
        setStatus("Please set the fileName");
        this.fileName = "";

        this.sliceX = null;
        this.sliceY = null;
        this.sliceZ = null;
        if (!this.currentFileScanner.isAlive()) {
            this.currentFileScanner.start();
        }
    }

    private Thread createCurrentFileScannerThread() {
        Runnable result = new Runnable() {

            @Override
            public void run() {
                while (true) {
                    DeviceProxy proxy;
                    try {
                        Thread.sleep(3000);
                        if (autoUpdateFileName) {
                            proxy = new DeviceProxy(dataRecorderDeviceName);

                            String folder = proxy.read_attribute("targetDirectory").extractString();
                            String fileName = proxy.read_attribute("fileName").extractString();
                            setFileName(fileName + ".nxs");
                            setFolderName(folder);
                        }
                        logger.debug("Thread End of Loop");
                    } catch (DevFailed e) {
                        String errorMessage = "Can't get current file name from the device DataRecorder ("
                                + dataRecorderDeviceName + ") .\n";
                        errorMessage += "Please check that the device " + dataRecorderDeviceName
                                + " is running or the property dataRecorderDeviceName is correct";
                        setState(DeviceState.FAULT);
                        setStatus(errorMessage);
                    } catch (InterruptedException e) {
                        logger.debug("Thread interrupted. Should never happen.");
                    }
                }

            }
        };

        return new Thread(result);
    }

    /**
     * delete device
     */
    @Delete
    public void delete() throws DevFailed {
        logger.debug("delete");
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Starts the server.
     */
    public static void main(final String[] args) {
        ServerManager.getInstance().start(args, HDFDataSlicer.class);
    }

}